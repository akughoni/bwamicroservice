const express = require('express');
const router = express.Router();
const mediaHandler = require("./handler/media");

/* GET home page. */
router.post('/store', mediaHandler.create);
router.get('/', mediaHandler.getAll);
router.delete('/delete/:id', mediaHandler.destroy);

module.exports = router;